Pod::Spec.new do |spec|

  spec.name          = "ZCPWebView"
  spec.version       = "0.0.1"
  spec.author        = { "朱超鹏" => "z164757979@163.com" }
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.homepage      = "https://gitlab.com/Malygos/ZCPWebView"
  spec.source        = { :git => "https://gitlab.com/Malygos/ZCPWebView.git", :tag => "#{spec.version}" }
  spec.summary       = "ZCP ZCPWebView."
  spec.description   = <<-DESC
                       ZCP ZCPWebView. pa hao fang router
                       DESC

  spec.platform      = :ios, '8.0'
  spec.ios.deployment_target = '8.0'
  spec.module_name   = 'ZCPWebView'
  spec.framework     = 'Foundation', 'UIKit'
  spec.source_files  = "ZCPWebView/ZCPWebView/**/*.{h,m}"

  spec.dependency 'ZCPGlobal', '~> 0.0.1'
  spec.dependency 'ZCPCategory', '~> 0.0.1'
  spec.dependency 'ZCPRouter', '~> 0.0.1'
  spec.dependency 'ZCPNetwork', '~> 0.0.1'
  spec.dependency 'ZCPUtil', '~> 0.0.1'

end
