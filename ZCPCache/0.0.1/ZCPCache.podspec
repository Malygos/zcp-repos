Pod::Spec.new do |spec|

  spec.name          = "ZCPCache"
  spec.version       = "0.0.1"
  spec.author        = { "朱超鹏" => "z164757979@163.com" }
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.homepage      = "https://gitlab.com/Malygos/ZCPCache"
  spec.source        = { :git => "https://gitlab.com/Malygos/ZCPCache.git", :tag => "#{spec.version}" }
  spec.summary       = "ZCP ZCPCache."
  spec.description   = <<-DESC
                       ZCP ZCPCache. pa hao fang router
                       DESC

  spec.platform      = :ios, '8.0'
  spec.ios.deployment_target = '8.0'
  spec.module_name   = 'ZCPCache'
  spec.framework     = 'Foundation', 'UIKit'
  spec.source_files  = "ZCPCache/ZCPCache/ZCPCache.h"

  spec.subspec 'CoreData' do |subspec_coredata|
    subspec_coredata.dependency 'MagicalRecord', '~> 2.3.2'
    subspec_coredata.framework = 'CoreData'
    subspec_coredata.source_files = 'ZCPCache/ZCPCache/CoreData/**/*.{h,m}'
  end
  
  spec.dependency 'ZCPGlobal', '~> 0.0.1'

end
