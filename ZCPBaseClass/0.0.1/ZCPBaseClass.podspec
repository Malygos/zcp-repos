Pod::Spec.new do |spec|

  spec.name          = "ZCPBaseClass"
  spec.version       = "0.0.1"
  spec.author        = { "朱超鹏" => "z164757979@163.com" }
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.homepage      = "https://gitlab.com/Malygos/ZCPBaseClass"
  spec.source        = { :git => "https://gitlab.com/Malygos/ZCPBaseClass.git", :tag => "#{spec.version}" }
  spec.summary       = "ZCP ZCPBaseClass."
  spec.description   = <<-DESC
                       ZCP ZCPBaseClass. pa hao fang router
                       DESC

  spec.platform      = :ios, '8.0'
  spec.ios.deployment_target = '8.0'
  spec.module_name   = 'ZCPBaseClass'
  spec.framework     = 'Foundation', 'UIKit'
  spec.source_files  = "ZCPBaseClass/ZCPBaseClass/**/*.{h,m}"

  spec.dependency 'Aspects', '~> 1.4.1'
  spec.dependency 'ZCPGlobal', '~> 0.0.1'
  spec.dependency 'ZCPCategory', '~> 0.0.1'
  spec.dependency 'ZCPUtil', '~> 0.0.1'

end
