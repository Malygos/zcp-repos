Pod::Spec.new do |s|

  s.name          = "ZCPKit"
  s.version       = "0.0.2"
  s.summary       = "It`s a framework."
  s.description   = <<-DESC
                      It`s a framework for myself.
                    DESC

  s.homepage      = "https://gitlab.com/Malygos/ZCPKit"
  s.license       = { :type => 'MIT', :file => 'LICENSE' }
  s.author        = { "朱超鹏" => "z164757979@163.com" }
  s.source        = { :git => "https://gitlab.com/Malygos/ZCPKit.git", :tag => "#{s.version}" }
  
  s.platform      = :ios, '8.0'

  s.source_files  = 'ZCPKit/ZCPKit.h'
  s.public_header_files = 'ZCPKit/ZCPKit.h'
  s.framework     = 'Foundation', 'UIKit'

# ――― Subspec ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.subspec 'ZCPUIKit' do |ss|
    ss.dependency 'ZCPKit/Category'
    ss.dependency 'ZCPKit/Global'
    ss.public_header_files = 'ZCPKit/ZCPUIKit/ZCPUIKit.h'
    ss.source_files  = 'ZCPKit/ZCPUIKit/ZCPUIKit.h'

    ss.subspec 'ZCPListView' do |sss|
      sss.dependency 'ZCPKit/BaseClass'
      sss.dependency 'ZCPKit/ZCPUIKit/EnhancedView'
      sss.public_header_files = 'ZCPKit/ZCPUIKit/ZCPListView/**/*.h'
      sss.source_files = 'ZCPKit/ZCPUIKit/ZCPListView/**/*.{h,m}'
    end

    ss.subspec 'ZCPWebView' do |sss|
      sss.dependency 'ZCPKit/Router'
      sss.dependency 'ZCPKit/Network'
      sss.dependency 'ZCPKit/Util'
      sss.public_header_files = 'ZCPKit/ZCPUIKit/ZCPWebView/**/*.h'
      sss.source_files = 'ZCPKit/ZCPUIKit/ZCPWebView/**/*.{h,m}'
    end

    ss.subspec 'EnhancedView' do |sss|
      sss.public_header_files = 'ZCPKit/ZCPUIKit/EnhancedView/**/*.h'
      sss.source_files = 'ZCPKit/ZCPUIKit/EnhancedView/**/*.{h,m}'
    end
  end

  s.subspec 'Router' do |ss|
    ss.dependency 'Aspects', '~>1.4.1'
    ss.dependency 'ZCPKit/Global'
    ss.dependency 'ZCPKit/BaseClass'
    ss.resources  = 'ZCPKit/Router/viewMap.plist'
    ss.public_header_files = 'ZCPKit/Router/**/*.h'
    ss.source_files = 'ZCPKit/Router/**/*.{h,m}'
  end

  s.subspec 'Network' do |ss|
    ss.dependency 'ZCPKit/Global'
    ss.public_header_files = 'ZCPKit/Network/**/*.h'
    ss.source_files = 'ZCPKit/Network/**/*.{h,m}'
  end

  s.subspec 'BaseClass' do |ss|
  	ss.dependency 'Aspects', '~>1.4.1'
    ss.dependency 'ZCPKit/Category'
    ss.dependency 'ZCPKit/Global'
    ss.dependency 'ZCPKit/Util'
    ss.source_files  = 'ZCPKit/BaseClass/**/*.{h,m}'
    ss.public_header_files = 'ZCPKit/BaseClass/**/*.h'
  end

  s.subspec 'Category' do |ss|
    ss.dependency 'ZCPKit/Global'
    ss.source_files = 'ZCPKit/Category/**/*.{h,m}'
    ss.public_header_files = 'ZCPKit/Category/**/*.h'
  end

  s.subspec 'Global' do |ss|
    ss.source_files = 'ZCPKit/Global/**/*.h'
    ss.public_header_files = 'ZCPKit/Global/**/*.h'
  end

  s.subspec 'Util' do |ss|
    ss.dependency 'ZCPKit/Global'
    ss.source_files = 'ZCPKit/Util/**/*.{h,m}'
    ss.public_header_files = 'ZCPKit/Util/**/*.h'
  end

  s.subspec 'Cache' do |ss|
    ss.dependency 'ZCPKit/Global'
    ss.source_files = 'ZCPKit/Cache/**/*.{h,m}'
    ss.public_header_files = 'ZCPKit/Cache/**/*.h'
    ss.subspec 'CoreData' do |sss|
      sss.dependency 'ZCPKit/Global'
      sss.dependency 'MagicalRecord'
      ss.framework = 'CoreData'
      sss.source_files = 'ZCPKit/Cache/CoreData/**/*.{h,m}'
      sss.public_header_files = 'ZCPKit/Cache/CoreData/**/*.h'
    end
  end
end