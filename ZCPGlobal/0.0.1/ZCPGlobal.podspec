Pod::Spec.new do |spec|

  spec.name          = "ZCPGlobal"
  spec.version       = "0.0.1"
  spec.author        = { "朱超鹏" => "z164757979@163.com" }
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.homepage      = "https://gitlab.com/Malygos/ZCPGlobal"
  spec.source        = { :git => "https://gitlab.com/Malygos/ZCPGlobal.git", :tag => "#{spec.version}" }
  spec.summary       = "ZCP ZCPGlobal."
  spec.description   = <<-DESC
                       ZCP ZCPGlobal. pa hao fang router
                       DESC

  spec.platform      = :ios, '8.0'
  spec.ios.deployment_target = '8.0'
  spec.module_name   = 'ZCPGlobal'
  spec.framework     = 'Foundation', 'UIKit'
  spec.source_files  = "ZCPGlobal/ZCPGlobal/**/*.{h,m}"

end
