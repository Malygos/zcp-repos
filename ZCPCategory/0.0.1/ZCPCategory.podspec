Pod::Spec.new do |spec|

  spec.name          = "ZCPCategory"
  spec.version       = "0.0.1"
  spec.author        = { "朱超鹏" => "z164757979@163.com" }
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.homepage      = "https://gitlab.com/Malygos/ZCPCategory"
  spec.source        = { :git => "https://gitlab.com/Malygos/ZCPCategory.git", :tag => "#{spec.version}" }
  spec.summary       = "ZCP Category."
  spec.description   = <<-DESC
                       ZCP Category. It has some Category
                       DESC

  spec.platform      = :ios, '9.0'
  spec.ios.deployment_target = '9.0'
  spec.module_name   = 'ZCPCategory'
  spec.framework     = 'Foundation', 'UIKit'

  # source_files start
  spec.source_files  = "Source/*.h"

  spec.subspec 'Foundation' do |subspec_foundation|
    subspec_foundation.source_files = "Source/Foundation/**/*.{h,m}"
  end

  spec.subspec 'UIKit' do |subspec_uikit|
    subspec_uikit.source_files = "Source/UIKit/**/*.{h,m}"
  end
  # source_files end

end
